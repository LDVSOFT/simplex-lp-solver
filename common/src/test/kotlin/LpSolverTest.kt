package net.ldvsoft.simplex_lp_solver

import kotlin.test.Test
import kotlin.test.assertTrue

class LpSolverTest {
    @Test
    fun simpleSolvableTest() {
        val x = LpVariable("x")
        val problem = LpProblem(
                listOf(x),
                listOf(LpConstraint(x, LpConstraintSign.LESS_OR_EQUAL, 2.0)),
                LpFunction(x, LpFunctionOptimization.MAXIMIZE)
        )

        val solution = solve(problem)
        assertTrue { solution is Solved }
        assertTrue { (solution as Solved).variablesValues[x]!! == 2.0 }
    }

    @Test
    fun simpleNonNegativeSolvableTest() {
        val x = LpVariable("x", canBeNegative = false)
        val problem = LpProblem(
                listOf(x),
                listOf(LpConstraint(x, LpConstraintSign.GREATER_OR_EQUAL, -3.0)),
                LpFunction(x, LpFunctionOptimization.MININIZE)
        )

        val solution = solve(problem)
        assertTrue { solution is Solved }
        assertTrue { (solution as Solved).variablesValues[x]!! == 0.0 }
    }

    @Test
    fun simpleNegativeSolvableTest() {
        val x = LpVariable("x", canBeNegative = true)
        val problem = LpProblem(
                listOf(x),
                listOf(LpConstraint(x, LpConstraintSign.GREATER_OR_EQUAL, -3.0)),
                LpFunction(x, LpFunctionOptimization.MININIZE)
        )

        val solution = solve(problem)
        assertTrue { solution is Solved }
        assertTrue { (solution as Solved).variablesValues[x]!! == -3.0 }
    }

    @Test
    fun simpleUnsolvableTest() {
        val x = LpVariable("x", canBeNegative = true)
        val problem = LpProblem(
                listOf(x),
                listOf(
                        LpConstraint(x, LpConstraintSign.GREATER_OR_EQUAL, 4.0),
                        LpConstraint(x, LpConstraintSign.LESS_OR_EQUAL, 3.0)
                ),
                LpFunction(x, LpFunctionOptimization.MININIZE)
        )

        assertTrue { solve(problem) == NoSolution }
    }


    @Test
    fun simpleUnboundedTest() {
        val x = LpVariable("x", canBeNegative = true)
        val problem = LpProblem(
                listOf(x),
                listOf(
                        LpConstraint(x, LpConstraintSign.GREATER_OR_EQUAL, 4.0)
                ),
                LpFunction(x, LpFunctionOptimization.MAXIMIZE)
        )

        assertTrue { solve(problem) == Unbounded }
    }
}