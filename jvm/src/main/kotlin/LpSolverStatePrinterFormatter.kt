package net.ldvsoft.simplex_lp_solver

internal actual object LpSolverStatePrinterFormatter {
    actual fun format(format: String, vararg args: Any): String = String.format(format, args)
}