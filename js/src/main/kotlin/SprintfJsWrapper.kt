@file:JsModule("sprintf-js")
@file:JsNonModule
package net.ldvsoft.simplex_lp_solver

external fun sprintf(format: String, vararg args: Any): String